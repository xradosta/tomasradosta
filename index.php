<?php
ini_set("xdebug.overload_var_dump", 1);

$a = htmlspecialchars($_GET["a"]);
$b = htmlspecialchars($_GET["b"]);
$c = htmlspecialchars($_GET["c"]);

$text = "Ahoj";
$cislostring = "10";
$bol = true;
$nl = NULL;
$float = 20.20;

echo "Ahoj <br>";
echo "Hodnota <b>A</b> je: <b>" . $a . "</b> <br>";
echo "Hodnota <b>B</b> je: <b>" . $b . "</b> <br>";
echo "Hodnota <b>C</b> je: <b>" . $c . "</b> <br>";

echo "Příklad " . $a . " * " . $c . " je: " . $a * $c;
echo "<br>";

//funkce
function vynasob($cislo)
{
    return $cislo * 2;
}

function zaokrouhleni($cislo, $mista)
{
    return round($cislo, $mista);
}

$vozidla["Osobni"]["Ford"]["Skoda"] = "RS";
$vozidla["Kamion"]["Iveco"]["Man"]["Daf"]["Scania"];
$vozidla["Nakladni"]["Iveco"]["Peugeot"]["Ford"] = "Transit";

echo "10 * 2 je: " . vynasob(10) . "<br>";
echo "(" . $a . "+" . $b . "+" . $c . ") * 2 je: " . vynasob($a + $b + $c) . "<br>";

echo zaokrouhleni($float, 1);
echo "<br>";

//porovnani
echo "<br> Porovnani <br>";
if ($a == $c) {
    echo "Hodnoty A a C jsou stejne";
} else if ($a > $c) {
    echo "Hodnota A neni stejna jako C a hodnota A je vetsi nez C";
} else {
    echo "Hodnota A a C nejsou stejne a hodnota A je mensi nez C";
}
echo "<br>";
echo "<br>";

//foreach
$array = array(1, 2, 3, 4, 5, "a", "b", "c");

foreach ($array as $vec => $polozka) {
    echo "Hodnota je: " . $polozka . "<br>";
}

echo "<br>";

function cena($bezdph, $dph = 15, $mena = "CZK")
{
    $dphcena = $bezdph * $dph;
    $dphcena = round($dphcena, 2);
    $dphcena = number_format($dphcena, 2, ".", " ");

    return $dphcena . " " . $mena;
}

echo cena(1854);
echo "<br>";
echo cena(9876, 21, "EUR");

echo "<br>";

//var dumps
var_dump($vozidla);
var_dump($vozidla["Nakladni"]);
var_dump($cislostring);
var_dump($text);
var_dump($bol);
var_dump($nl);
var_dump($float);

var_dump($a + $b);
var_dump($a - $b);
var_dump($a * $b);
var_dump($a / $b);

var_dump($a++);
var_dump($a--);

var_dump($a == $b);
var_dump($a === $b);
var_dump($a !== $b);

var_dump($a += $b);
var_dump($a *= $b);
var_dump($a /= $b);
?>

<html>
<button onclick="ShowVideo()">Zorazit</button>
<button onclick="HideVideo()">Skryt</button>
<style>
    .hide {
        opacity: 0;
    }

    .show {
        opacity: 1;
    }
</style>
<video id="video" src="uni.webm" width="80%" controls class="hide">
    <br>
    <script>
        function ShowVideo() {
            document.getElementById("video").setAttribute("class", "show");
        }

        function HideVideo() {
            document.getElementById("video").setAttribute("class", "hide");
        }
    </script>
</html>