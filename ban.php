<?php

$text = "Toto je věta, která je vhodná pro fóra a neobsahuje žádné zakázané slovo.";
$ne = array("škola", "rýže", "Koronavirus", "slovo");

var_dump(isValid($text, $ne));

function isValid($veta, $zakazane)
{
    foreach ($zakazane as $slovo) {
        $status = strpos($veta, $slovo);
        if ($status !== false) return false;
    }
    return true;
}
